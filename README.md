# Graphs in Reason

A small project for me to learn more about Reason + Reason-React.

Right now, `DataGraph.re` contains the data structure to perform searches.

A few components exist to perform and visualize the search.