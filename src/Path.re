let component = ReasonReact.statelessComponent("Path");

let make = (~dest, ~path, _children) => {
  {
    ...component,
    render: (_self) => {
      let toPathList = (i, node: DataGraph.node) => {
        let text = if (node.value != dest) { " -> " } else { "" };
        let selectedColor = "#" ++ Colors.colors[i];

        <span>
          <span style=(ReactDOMRe.Style.make(~color=selectedColor, ()))>
            (Utils.s(node.value))
          </span>
          <span> (Utils.s(text)) </span>
        </span>
      };

      let values = path
        |> Array.of_list
        |> Array.mapi(toPathList);

      <div>
        <p>(Utils.s("Try this: "))</p>
        (ReasonReact.createDomElement("div", ~props={"className": "Path"}, values))
      </div>
    }
  }
};