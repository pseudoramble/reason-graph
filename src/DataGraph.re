module StrMap = Map.Make(String);

type node = {
  value: string,
  edges: list(string)
};

type graph = 
  | Graph(list(node));

let _node = (value) => {
  value: value,
  edges: []
};

let _nodeOfValue = (v, n) => n.value == v;
let _findNode = (v, nodeList) => List.find(_nodeOfValue(v), nodeList);
let _updateNode = (source, dest, nodeList) => {
  List.filter((node) => node.value !== source.value, nodeList)
  |> filteredNodes => [{ ...source, edges: [dest.value, ...source.edges] }, ...filteredNodes];
};
let _addIfNew = (value, lst) => if (List.exists(_nodeOfValue(value), lst)) lst else [_node(value), ...lst];

let _addEdge = ((source, dest), nodes) => {
  let withSourceAndDest = nodes
    |> _addIfNew(source)
    |> _addIfNew(dest);

  let sourceNode = _findNode(source, withSourceAndDest);
  let destNode = _findNode(dest, withSourceAndDest);

  _updateNode(sourceNode, destNode, withSourceAndDest);
};

let init = (definitions) => {
  let map = List.fold_right(_addEdge, definitions, []);

  Graph(map);
};

let nodes = (graph) => {
  switch graph {
  | Graph(m) => m
  }
};

let assoc = (source, dest, graph) => {
  let nodeList = nodes(graph);

  if (List.exists(_nodeOfValue(source), nodeList) && List.exists(_nodeOfValue(dest), nodeList)) {
    let sourceNode = _findNode(source, nodeList);
    let destNode = _findNode(dest, nodeList);
  
    Graph(_updateNode(sourceNode, destNode, nodeList));
  } else {
    raise(Not_found);
  };
};

let mem = (key, graph) => {
  let nodeList = nodes(graph);
  List.exists(_nodeOfValue(key), nodeList);
};

let path = (source, dest, graph) => {
  let nodeList = nodes(graph);

  let rec pathAux = (cur, dest, path) => {
    if (List.exists((n) => n === cur, path)) {
      None;
    } else if (cur.value === dest.value) {
      Some([dest, ...path]);
    } else {
      let edges = cur.edges;

      if (List.length(edges) == 0) {
        None;
      } else {
        List.fold_right((next, result) => {
          switch result {
          | Some(r) => Some(r);
          | None => pathAux(_findNode(next, nodeList), dest, [cur, ...path])
          };
        }, edges, None);
      };
    };
  };
  
  if (mem(source, graph) && mem(dest, graph)) {
    let sourceNode = _findNode(source, nodeList);
    let destNode = _findNode(dest, nodeList);

    switch (pathAux(sourceNode, destNode, [])) {
    | Some(path) => Some(List.rev(path));
    | _ => None
    }
  } else {
    None;
  }
};