[%bs.raw {|require('./graph.css')|}];

let s = ReasonReact.stringToElement;
let component = ReasonReact.statelessComponent("Graph");

let shamefullyFindPathIndex = (value, path) => {
  let pathIndex = ref(-1);
  List.iteri((index, node: DataGraph.node) => { 
    if (node.value == value) {
      pathIndex := index;
    }
   }, path);

  if (pathIndex^ != -1) {
    Some(pathIndex^)
  } else {
    None
  };
};

let make = (~dataGraph, ~path, _children) => {
  ...component,
  render: (_self) => {
    let values = DataGraph.nodes(dataGraph)
      |> Array.of_list
      |> Array.map((node: DataGraph.node) => {
        let pathIndex = switch path {
        | Some(p) => shamefullyFindPathIndex(node.value, p)
        | None => None
        };

        <GraphNode pathIndex=(pathIndex) value=(node.value) edges=(node.edges) />
      });

    ReasonReact.createDomElement("div", ~props={"className": "Graph"}, values);
  }
};
