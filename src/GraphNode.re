let component = ReasonReact.statelessComponent("GraphNode");
let s = ReasonReact.stringToElement;

let make = (~value, ~edges, ~pathIndex, _children) => {
  ...component,
  render: (_self) => {
    let color = switch pathIndex {
    | Some(index) => Colors.colors[index]
    | None => "d6d6d6"
    };

    <div className="Graph-node">
      <div style=(ReactDOMRe.Style.make(~backgroundColor=("#" ++ color), ())) className="Graph-node_value">
        (s(value))
      </div>
      (
        Array.of_list(edges)
        |> Array.map((value) => <div className="Graph-node_edge"> (s(value)) </div>)
        |> ReasonReact.createDomElement("div", ~props={"className": "Graph-node_edges"})
      )
    </div>
  }
};