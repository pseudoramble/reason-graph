[%bs.raw {|require('./app.css')|}];

[@bs.module] external logo : string = "./logo.svg";

type action = 
  | Search(string, string);
type state = {
  source: string,
  dest: string,
  path: option(list(DataGraph.node))
};

let s = Utils.s;
let component = ReasonReact.reducerComponent("App");

let graph = DataGraph.init([
  ("A", "B"),
  ("A", "C"),
  ("B", "C"),
  ("C", "D"),
  ("E", "F"),
  ("F", "A"),
  ("A", "F"),
  ("A", "E"),
]);

let make = (_children) => {
  ...component,
  initialState: () => { source: "", dest: "", path: None },
  reducer: (action, _state) => {
    switch action {
    | Search(source, dest) => ReasonReact.Update({ source, dest, path: DataGraph.path(source, dest, graph) })
    }
  },
  render: (self) =>
    <div className="App">
      <p className="App-intro">
        <Graph dataGraph={graph} path={self.state.path} />
        (
          switch self.state.path {
          | None => <p>(s("Give me a source/dest, I'll find you a way!"))</p>
          | Some(p) => <Path dest=(self.state.dest) path=(p) />
          }
        )
        <FindPathForm submit={self.reduce(((source, dest)) => Search(source, dest))} />
      </p>
    </div>
};
