let join = (values,  ~separator=",") => 
  List.fold_right((cur, str) => str ++ cur ++ separator, values, "");

let s = ReasonReact.stringToElement;