type state = { source: string, dest: string };
type action = 
  | UpdateSource(string)
  | UpdateDest(string);

let component = ReasonReact.reducerComponent("FindPathForm");

let make = (~submit, _children) => {
  let onSubmit = (_event, self: ReasonReact.self('state, 'payload, 'action)) => {
    submit((self.state.source, self.state.dest));
  };
  {
    ...component,
    initialState: () => {
      source: "",
      dest: ""
    },
    reducer: (action, state) => {
      switch action {
      | UpdateSource(value) => ReasonReact.Update({ ...state, source: value })
      | UpdateDest(value) => ReasonReact.Update({ ...state, dest: value })
      }
    },
    render: (self) => {
      <div>
        <input placeholder="Start" onChange={self.reduce(event => UpdateSource(ReactDOMRe.domElementToObj(ReactEventRe.Form.target(event))##value))} />
        <input placeholder="End" onChange={self.reduce(event => UpdateDest(ReactDOMRe.domElementToObj(ReactEventRe.Form.target(event))##value))} />
        <button onClick={self.handle(onSubmit)}> (Utils.s("Search")) </button>
      </div>
    }
  }
};